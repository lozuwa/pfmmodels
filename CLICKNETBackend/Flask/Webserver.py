# Flask
from flask import Flask, render_template, request, redirect, url_for, send_from_directory, Response, jsonify
from werkzeug import secure_filename
import numpy as np
# Set cors
from flask_cors import CORS, cross_origin
# Threads
import eventlet
eventlet.monkey_patch()
# db
from pymongo import *
# Others
import sys, os, datetime

# Start CORS 
app = Flask(__name__)
CORS(app)

try:
	client = MongoClient()
	db = client.click
except:
	raise Exception('Is mongodb running?')

# Upload folders
app.config['UPLOAD_FOLDER_Ascaris'] = '/home/rodrigoloza/Dropbox/CLICK_Medical/Databases/A.Lumbricoides/'
app.config['UPLOAD_FOLDER_Blastos'] = '/home/rodrigoloza/Dropbox/CLICK_Medical/Databases/B.Hominis/'
app.config['UPLOAD_FOLDER_E.Coli'] = '/home/rodrigoloza/Dropbox/CLICK_Medical/Databases/E.Coli/'
app.config['UPLOAD_FOLDER_GiardiaLamblia'] = '/home/rodrigoloza/Dropbox/CLICK_Medical/Databases/G.Lamblia/'
app.config['UPLOAD_FOLDER_Artefactos'] = '/home/rodrigoloza/Dropbox/CLICK_Medical/Databases/Artefactos/'

# Allowed extensions
app.config['ALLOWED_EXTENSIONS'] = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])

# Check extension of file
def allowed_file(filename):
	return '.' in filename and filename.rsplit('.', 1)[1] in app.config['ALLOWED_EXTENSIONS']

# Timestamp
def timestamp():
  now = datetime.datetime.now()
  return str(now.hour) + str(now.minute) + str(now.second) + str(now.microsecond)

# Main page
@app.route('/', methods=['GET'])
def index():
	return render_template('index.html')

# Post image
@app.route('/post_image', methods=['POST', 'GET'])
def post_image():
	if request.method == 'GET':
		return render_template('PostFile.html')
	elif request.method == 'POST':
		file = request.files['file']
		#print(file)
		# Request Multi-part file 
		file = request.files['file']

		# If file is allowed, continue
		if file and allowed_file(file.filename):
			filename = secure_filename(file.filename)
		else:
			raise Exception('File is not allowed')
			sys.exit()

		# Get name of the file and save it
		filename = str(timestamp()+'_'+filename)
		name = filename.split('_')[1].split('.')[0]

		if name == 'ECOLI':
			file.save( os.path.join(app.config['UPLOAD_FOLDER_E.Coli'], filename) )
			db.click.update({'_id':'E.Coli'}, {'$push':{'urls':filename}})

		elif name == 'Blastos':
			file.save( os.path.join(app.config['UPLOAD_FOLDER_Blastos'], filename) )
			db.click.update({'_id':'Blastos'}, {'$push':{'urls':filename}})

		elif name == 'GiardiaLamblia':
			file.save( os.path.join(app.config['UPLOAD_FOLDER_GiardiaLamblia'], filename) )
			db.click.update({'_id':'GiardiaLamblia'}, {'$push':{'urls':filename}})

		elif name == 'Ascaris':
			file.save( os.path.join(app.config['UPLOAD_FOLDER_Ascaris'], filename) )
			db.click.update({'_id':'Ascaris'}, {'$push':{'urls':filename}})

		elif name == 'Artefactos':
			file.save( os.path.join(app.config['UPLOAD_FOLDER_Artefactos'], filename) )
			db.click.update({'_id':'Artefactos'}, {'$push':{'urls':filename}})
		else:
			raise Exceptions('Filename not in db')
			return jsonify({'result': 'failed'})

		return jsonify({'result': 'success'})

	else:
		return '''<b> Method not supported </b>'''

#eventlet.spawn(listen)

if __name__ == '__main__':
    # Run webserver
    app.run(host="192.168.3.174",
    port=int("5000"),
    debug=True)
