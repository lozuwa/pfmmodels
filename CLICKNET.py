# Author: Rodrigo Loza 
# Company: pfm Medical Bolivia 
# Description: CLICKNET is a retrained instance of inception v-3. The CNN has been retrained
# to classify parasite images (E.COLI). 
# The CLICKNET class loads the retrained inception-v3 graph. Contains functions to:
# 1. Predict the output of a single input image.   
# 2. Own implementation RCNN

import os, sys, cv2
from PIL import Image 
import numpy as np
from numpy import c_, r_
import tensorflow as tf
import matplotlib.pyplot as plt 
from preprocessing import preprocess_GRID

class CLICKNET:
	def __init__(self, verbosity = False):
		# Parameters
		# Verbosity -> Boolean type. Sets the verbosity function of prediction. 
		self.verbosity = verbosity
		#self.label_lines =  [line.rstrip() for line in tf.gfile.GFile("c:/Users/HP/Dropbox/CLICK_Medical/Databases/CLICKNET/GRAPHS/retrained_labels.txt")]
		self.label_lines =  [line.rstrip() for line in tf.gfile.GFile(os.getcwd()+"/GRAPHS/retrained_labels.txt")]

		#with tf.gfile.FastGFile("c:/Users/HP/Dropbox/CLICK_Medical/Databases/CLICKNET/GRAPHS/retrained_graph.pb", 'rb') as f:
		with tf.gfile.FastGFile(os.getcwd()+"/GRAPHS/retrained_graph.pb", 'rb') as f:
			self.graph_def = tf.GraphDef()
			self.graph_def.ParseFromString(f.read())
			self.__ = tf.import_graph_def(self.graph_def, name='')
		self.sess = tf.Session()
		self.softmax_tensor = self.sess.graph.get_tensor_by_name('final_result:0')

	def predict(self, image_path):
		# Parameters 
		# image_path -> str type. Path to the input image 
		image_data = tf.gfile.FastGFile(image_path, 'rb').read()
		predictions = self.sess.run(self.softmax_tensor, {'DecodeJpeg/contents:0': image_data})
		top_k = predictions[0].argsort()[-len(predictions[0]):][::-1]
		return_dict = {}
		for node_id in top_k[:5]:
			human_string = self.label_lines[node_id]
			score = predictions[0][node_id]
			return_dict[human_string] = score
			if self.verbosity:
				print('%s (score = %.5f)' % (human_string, score))
		return return_dict

	def RCNN(self, image_path, slSize, stride, padding):
		# Parameteres 
		# image_path -> str type. Path to the input image 
		# slSize -> tuple type (h,w). Size of the sliding window 
		# stride -> tuple type (h,w). Sliding window movement coefficients
		# padding -> str type ('VALID'). Fixed padding.

		original = cv2.imread(image_path)
		frame = Image.open(image_path)
		width, height = frame.size 

		print('Preprocessing patches .....')
		if padding == 'VALID':
			patches, ch, cs = preprocess_GRID(fw = width, fh = height, slSize = slSize , stride = stride, padding = padding)
		elif padding == 'SAME':
			patches, ch, cs, zeros_h, zeros_w = preprocess_GRID(fw = width, fh = height, slSize = slSize , stride = stride, padding = padding)
			rows, cols, d = original.shape
			# If height is even or odd
			if (zeros_h % 2 == 0):
				zeros_h = int(zeros_h/2)
				original = r_[np.zeros((zeros_h, cols, 3)), original, np.zeros((zeros_h, cols, 3))]
			else:
				zeros_h += 1
				zeros_h = int(zeros_h/2)
				original = r_[np.zeros((zeros_h, cols, 3)), original, np.zeros((zeros_h, cols, 3))]

			rows, cols, d = original.shape
			# If width is even or odd 
			if (zeros_w % 2 == 0):
				zeros_w = int(zeros_w/2)
				# Container 
				container = np.zeros((rows,(zeros_w*2+cols),3), np.uint8)
				container[:,zeros_w:container.shape[1]-zeros_w:,:] = original
				original = container #c_[np.zeros((rows, zeros_w)), original, np.zeros((rows, zeros_w))]
			else:
				zeros_w += 1
				zeros_w = int(zeros_w/2)
				container = np.zeros((rows,(zeros_w*2+cols),3), np.uint8)
				container[:,zeros_w:container.shape[1]-zeros_w:,:] = original
				original = container #c_[np.zeros((rows, zeros_w, 3)), original, np.zeros((rows, zeros_w, 3))]
		else:
			raise Exception('padding parameter should be specified')

		print('Analizing patches .....')
		activationMap = np.zeros(original.shape, np.uint8)
		cv2.namedWindow('HeatMap', cv2.WINDOW_NORMAL)
		cv2.namedWindow('Original', cv2.WINDOW_NORMAL)
		cv2.resizeWindow('HeatMap', 640, 550)
		cv2.resizeWindow('Original', 640, 550)
		patch_path = os.getcwd()+'/tmp/temp_img.jpg'

		number_patches = ch*cs
		for patch in patches:
			# Extract patch and crop image 
			starth, startw, kh, kw = patch[0], patch[1], patch[2], patch[3]
			frame.crop((startw, starth, kw, kh)).save(patch_path)

			# Classify patch 
			val = self.predict( patch_path ).get('parasite')
			activationMap[starth:kh, startw:kw, :] = (200, 128, int(val*255))

			# Visual feedback
			if val > 0.8:
				cv2.rectangle(original, (startw, starth), (kw, kh), (0, 255, 0), 8)
			else:
				cv2.rectangle(original, (startw, starth), (kw, kh), (0, 0, 255), 8)

			# Display patch and info
			print('Missing patches: ', number_patches, ' Parasite: ', val)
			cv2.imshow('HeatMap', activationMap)
			cv2.imshow('Original', original)
			cv2.waitKey(50)
			number_patches -= 1

		os.remove(patch_path)
		cv2.imwrite('tmp/Heatmap'+image_path.split('.')[-2].split('/')[-1]+'.jpg', activationMap)
		cv2.imwrite('tmp/Grid'+image_path.split('.')[-2].split('/')[-1]+'.jpg', original)

		print('Press any key to continue ...')
		cv2.waitKey(0)
