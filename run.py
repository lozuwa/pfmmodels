from CLICKNET import CLICKNET
import os, sys

if sys.argv[1] == '0':
	file = open('resultsArtifacts.csv', 'w')

	pathP = '/home/HP/Dropbox/CLICK_Medical/Databases/E.COLI/db/Parasite'
	pathA = '/home/HP/Dropbox/CLICK_Medical/Databases/E.COLI/db/Artifacts'

	if sys.argv[1] == 'Parasite':
	 path = pathP
	 names = ['Parasite', 'Artifact']
	else:
	 path = pathA
	 names = ['Artifact', 'Parasite']

	imgs = os.listdir(path)
	imgs.sort()
	nn = CLICKNET(verbosity=False)

	for img in imgs:
	 p = [(a,b) for a,b in nn.predict(path+'/'+img).items()]
	 if p[0][1] < 0.95:
	  print(img, '   ', p)
	 file.write(str(img) + '  ' + names[0] + str(p[0][1]) + ' '+ names[1] +str(p[1][1])+'\n')

	file.close()

elif sys.argv[1] == '1':
	print('RCNN')
	nn = CLICKNET(verbosity=False)
	nn.RCNN(image_path = '/home/rodrigoloza/Dropbox/CLICK_Medical/Databases/E.Coli/24157000_ECOLI.jpg', slSize=(401,302), stride=(206,151), padding='SAME')
	#nn.RCNN(image_path = 'C:/Users/HP/Dropbox/CLICK_Medical/Databases/E.Coli/24982463_ECOLI.jpg', slSize = (412, 302), stride = (206, 151), padding = 'SAME')

