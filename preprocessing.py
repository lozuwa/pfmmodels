# Author: Rodrigo Loza 
# Company: pfm Medical Bolivia 
# Description: preprocessing implements:
# 1. preprocess_GRID -> Sliding window with strides and kernel hyperparameters tunable
# 2. lazy_SAMEpad -> pad image when SAME has been called

# TO FIX 
# 1. Lines (63,64) -> when applying same padding, we return the amount of patches 
# that should exist in the height and width sizes. There is always one missing. 
# Calculate this value properly, so we don't have to add +1 to the next fors.  

import numpy as np
from numpy import r_, c_
import sys, os, cv2, math 
from PIL import Image

#def preprocess_GRID(fw = 3096, fh = 4128, slSize = (532, 403), stride = (5,5), padding = 'VALID'):
def preprocess_GRID(fw, fh, slSize, stride, padding):
	# Parameters
	# fw, fh -> int types. Size of the image  
	# slSize -> tuple type (height, width). Size of the sliding window
	# stride -> tuple type (height, width). Stride size for each direction
	# padding -> str type ('VALID' implemented, 'SAME' not implemented). Padding 
	
	kh, kw  = slSize[0], slSize[1]
	sh, sw = stride[0], stride[1]
	if (kh > fh) or (kw > fw):
		raise ValueError(' Sliding window\'s size  cannot be bigger than image\'s size ')
		sys.exit()
	elif (sh > fh) or (sw > fw):
		raise ValueError(' Strides\' size cannot be bigger than the image\'s size ')
		sys.exit()
	else:
		if padding == 'VALID':
			starth = 0
			startw = 0
			patches = []
			ch, cs = getValidPadding(kh, sh, fh, kw, sw, fw)
			print('ch: ', ch, 'cs: ', cs)
			for i in range(ch):
				for j in range(cs):
					patches.append( [starth, startw, kh, kw] )
					# Update width with strides 
					startw += sw
					kw += sw
				# Re-initialize the width parameters 
				startw = 0
				kw = slSize[1]
				# Update height with strides 
				starth += sh 
				kh += sh
			return patches, ch, cs

		elif padding == 'SAME':
			starth = 0
			startw = 0
			patches = []
			# Modify image tensor 
			zeros_h, zeros_w = getSamePadding(kh, sh, fh, kw, sw, fw)
			fw += zeros_w
			fh += zeros_h
			# Valid padding should fit exactly 
			ch, cs = getValidPadding(kh, sh, fh, kw, sw, fw)
			#######################TOFIX############################
			for i in range(ch+1):
				for j in range(cs+1):
			########################################################
					patches.append( [starth, startw, kh, kw] )
					# Update width with strides 
					startw += sw
					kw += sw
				# Re-initialize width parameters
				startw = 0
				kw = slSize[1]
				# Update height with strides 
				starth += sh
				kh += sh
			#######################TOFIX############################
			return patches, ch+1, cs+1, zeros_h, zeros_w 
			########################################################

		else:
			return None, None
	
def getValidPadding(kh, sh, fh, kw, sw, fw):
	ch_ = 0
	cs_ = 0
	while(kh < fh):
		kh += sh
		ch_ += 1
	while(kw < fw):
		kw += sw
		cs_ += 1
	return ch_, cs_

def getSamePadding(kh, sh, fh, kw, sw, fw):
	aux_kh = kh
	aux_kw = kw
	ch_ = 0
	cs_ = 0
	while(fh > kh):
		kh += sh
		ch_ += 1
	while(fw > kw):
		kw += sw
		cs_ += 1
	# Pixels left that do not fit in the kernel 
	resid_h = fh - (kh-sh)
	resid_w = fw - (kw-sw)
	# Amount of zeros to add to the image 
	zeros_h = aux_kh - resid_h
	zeros_w = aux_kw - resid_w
	#print(kh, fh, resid_h, zeros_h)
	# Return amount 
	return zeros_h, zeros_w

def lazy_SAMEpad(frame, zeros_h, zeros_w):
	rows, cols, d = frame.shape
	# If height is even or odd
	if (zeros_h % 2 == 0):
		zeros_h = int(zeros_h/2)
		frame = r_[np.zeros((zeros_h, cols, 3)), frame, np.zeros((zeros_h, cols, 3))]
	else:
		zeros_h += 1
		zeros_h = int(zeros_h/2)
		frame = r_[np.zeros((zeros_h, cols, 3)), frame, np.zeros((zeros_h, cols, 3))]

	rows, cols, d = frame.shape
	# If width is even or odd 
	if (zeros_w % 2 == 0):
		zeros_w = int(zeros_w/2)
		# Container 
		container = np.zeros((rows,(zeros_w*2+cols),3), np.uint8)
		container[:,zeros_w:container.shape[1]-zeros_w:,:] = frame
		frame = container #c_[np.zeros((rows, zeros_w)), frame, np.zeros((rows, zeros_w))]
	else:
		zeros_w += 1
		zeros_w = int(zeros_w/2)
		container = np.zeros((rows,(zeros_w*2+cols),3), np.uint8)
		container[:,zeros_w:container.shape[1]-zeros_w:,:] = frame
		frame = container #c_[np.zeros((rows, zeros_w, 3)), frame, np.zeros((rows, zeros_w, 3))]

	return frame